// @flow

import * as React from 'react';
import { Component } from 'react-simplified';
import { ArticlePage, PageButtons, ArticleCard, Alert, ArticleComments, ArticleEditor } from '../src/widgets.js';
import { shallow, mount } from 'enzyme';
import { type Category, type Importance, type Article } from '../src/services.js';

describe('Alert tests', () => {
	const wrapper = shallow(<Alert />);

	it('initially', () => {
		let instance: ?Alert = Alert.instance();
		expect(typeof instance).toEqual('object');
		if (instance) expect(instance.alerts).toEqual([]);

		expect(wrapper.find('button.close')).toHaveLength(0);
	});

	it('after danger', (done) => {
		Alert.danger('test');

		setTimeout(() => {
			let instance: ?Alert = Alert.instance();
			expect(typeof instance).toEqual('object');
			if (instance) expect(instance.alerts).toEqual([ { text: 'test', type: 'danger' } ]);

			expect(wrapper.find('button.close')).toHaveLength(1);

			done();
		});
	});

	it('after clicking close button', () => {
		wrapper.find('button.close').simulate('click');

		let instance: ?Alert = Alert.instance();
		expect(typeof instance).toEqual('object');
		if (instance) expect(instance.alerts).toEqual([]);

		expect(wrapper.find('button.close')).toHaveLength(0);
	});
});

let comments = [
	{
		name: 'bjørnar',
		body: 'kul artikkel',
		likes: 3,
		comment_id: 1,
		time: '01.01.2018, 12:00'
	}
];

function postHandler(comment: Object, callback: boolean => mixed) {
	comments = [
		{
			name: comment.name,
			body: comment.body,
			likes: 0,
			comment_id: 2,
			time: '01.01.2018, 12:00'
		}
	].concat(comments);
	callback(true);
}

describe('Comment tests', () => {
	const wrapper = mount(<ArticleComments comments={comments} postHandler={postHandler} likeHandler={null} />);

	it('check values', () => {
		let instance = wrapper.instance();

		expect(instance.props.comments.length).toBe(1);
		expect(wrapper.find('ul.list-group').children()).toHaveLength(1);
		expect(wrapper.find('ul.list-group').childAt(0).find('b').text()).toEqual(comments[0].name);
		expect(instance.comment).toEqual({ name: '', body: '' });
	});

	it('post invalid comment', () => {
		let instance = wrapper.instance();

		instance.comment = { name: '', body: '' };
		expect(instance.postComment()).toBe(false); //empty fields

		instance.comment = { name: 'Per Hansen', body: '' };
		expect(instance.postComment()).toBe(false); //valid name, invalid body

		instance.comment = { name: '', body: 'Kul artikkel' };
		expect(instance.postComment()).toBe(false); //valid body, invalid name
	});

	it('post valid comment', () => {
		let instance = wrapper.instance();

		instance.comment = { name: 'per', body: 'a' }; //minimum valid comment
		expect(instance.postComment()).toBe(true);
	});
});

function submitArticle(article: Object, callback: () => mixed) {}

describe('Article editor tests', () => {
	const wrapper = mount(
		<ArticleEditor
			categories={[ { category_id: 1, name: 'News', path: 'news' } ]}
			importances={[ { importance_id: 1, name: 'Normal' } ]}
			back={null}
			submit={submitArticle}
		/>
	);

	it('post invalid article', () => {
		let instance = wrapper.instance();

		instance.article.title = '';
		instance.article.img = '';
		instance.article.body = '';
		expect(instance.submit()).toBe(false); //Empty fields

		instance.article.title = 'Kul artikkel';
		instance.article.img = 'beklager har ikke bilde';
		instance.article.body =
			'dette er en veldig kul artikkel som jeg synes er veldig godt skrevet og akkurat over 100 bokstaver :)';
		expect(instance.submit()).toBe(false); //Invalid img url

		instance.article.title = '1234';
		instance.article.img = 'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png';
		instance.article.body =
			'dette er en veldig kul artikkel som jeg synes er veldig godt skrevet og akkurat over 100 bokstaver :)';
		expect(instance.submit()).toBe(false); //Too short title

		instance.article.title = 'Kul artikkel';
		instance.article.img = 'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png';
		instance.article.body =
			'dette er en veldig kul artikkel som jeg synes er veldig godt skrevet og akkurat under 100 bokstaver';
		expect(instance.submit()).toBe(false); //Too short body
	});

	it('post valid article', () => {
		let instance = wrapper.instance();

		instance.article.title = 'Kul artikkel';
		instance.article.img = 'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png';
		instance.article.body =
			'dette er en veldig kul artikkel som jeg synes er veldig godt skrevet og akkurat over 100 bokstaver :)';
		expect(instance.submit()).toBe(true);
	});
});

let article1: Article = {
	article_id: 1,
	cName: 'news',
	category_id: 1,
	importance_id: 2,
	title: 'Kul artikkel',
	body: '',
	img: 'http://...png',
	img_x: 50,
	img_y: 50,
	likes: 5,
	updated: '',
	html: 0,
	created: '2018-11-20T11:00:00.000Z'
};

describe('ArticleCard tests', () => {
	const wrapper = shallow(<ArticleCard article={article1} preview={false} />);

	it('check values', () => {
		expect(wrapper.find('a')).toBeDefined();
		expect(wrapper.find('img').prop('src')).toEqual(article1.img);
		expect(wrapper.find('h3').text()).toEqual(article1.title);
		expect(wrapper.find('span').at(0).text()).toEqual(article1.likes + ' likes');
	});
});

describe('PageButtons tests', () => {
	const wrapper = shallow(<PageButtons history={null} home={'/'} nElements={1000} prPage={10} curPage={20} />);

	it('check values', () => {
		expect(wrapper.find('button').at(0).text()).toEqual('1'); //First page
		expect(wrapper.find('button').at(1).text()).toEqual('17');
		expect(wrapper.find('button').at(2).text()).toEqual('18');
		expect(wrapper.find('button').at(3).text()).toEqual('19');
		expect(wrapper.find('button').at(4).text()).toEqual('20'); //Current page
		expect(wrapper.find('button').at(5).text()).toEqual('21');
		expect(wrapper.find('button').at(6).text()).toEqual('22');
		expect(wrapper.find('button').at(7).text()).toEqual('23');
		expect(wrapper.find('button').at(8).text()).toEqual('100'); //Last page
	});
});

let article2: Article = {
	article_id: 1,
	cName: 'news',
	category_id: 1,
	importance_id: 2,
	title: 'Kul artikkel',
	img: 'http://...png',
	img_x: 50,
	img_y: 50,
	likes: 5,
	body: 'veldig veldig kul artikkel hvis jeg må si det selv',
	html: 0,
	updated: '',
	created: '2018-11-20T11:00:00.000Z'
};

describe('ArticlePage tests', () => {
	const wrapper = shallow(
		<ArticlePage
			article_id={1}
			category={'news'}
			article={article2}
			preview={false}
			likeHandler={null}
			deleteHandler={null}
		/>
	);

	it('check values', () => {
		expect(wrapper.find('img').prop('src')).toEqual(article2.img);
		expect(wrapper.find('h1').text()).toEqual(article2.title);
		expect(wrapper.find('button').at(0).text()).toEqual('Like ' + article2.likes);
		expect(wrapper.find('.articleBody').text()).toEqual(article2.body);
	});
});
