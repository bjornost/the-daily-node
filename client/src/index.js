// @flow

import ReactDOM from 'react-dom';
import queryString from 'query-string';
import * as React from 'react';
import { Component } from 'react-simplified';
import { Router, Route, NavLink, Switch, Redirect, Link } from 'react-router-dom';
import { type Article, articleService, categoryService, importanceService, commentService } from './services';
import {
	LiveFeed,
	ArticleCard,
	PageButtons,
	ArticleColumns,
	ArticleEditor,
	ArticlePage,
	ArticleComments,
	Alert,
	Menu,
	Footer
} from './widgets';

// Reload application when not in production environment
if (process.env.NODE_ENV !== 'production') {
	let script = document.createElement('script');
	script.src = '/reload/reload.js';
	if (document.body) document.body.appendChild(script);
}

import createBrowserHistory from 'history/createBrowserHistory';
const history: any = createBrowserHistory();

history.listen((location) => {
	window.scrollTo(0, 0);
	Alert.clear();
});

class ArticleListRoute extends Component<{ match: { params: { category: string } } }> {
	articles: Article[] = [];
	loaded = false;
	nTotal = 0;
	prPage = 12;
	curPage = 1;
	home = '/';
	catPage = false;

	render() {
		if (this.articles.length < 1 && this.loaded) {
			return <Redirect to="/404" />;
		}
		if (
			(this.nTotal > 0 && this.curPage > Math.ceil(this.nTotal / this.prPage)) ||
			this.curPage < 1 ||
			isNaN(this.curPage)
		)
			return <Redirect to="/404" />;
		return (
			<div className="contentWrapper">
				<ArticleColumns articles={this.articles} />
				<PageButtons
					home={this.home}
					history={history}
					prPage={this.prPage}
					nElements={this.nTotal}
					curPage={this.curPage}
				/>
			</div>
		);
	}

	updated() {
		let params = queryString.parse((this.props: any).location.search);
		let curPage = params.page ? parseInt(params.page) : 1;
		if (this.curPage != curPage) {
			this.mounted();
		}
	}

	mounted() {
		let params = queryString.parse((this.props: any).location.search);
		this.curPage = params.page ? parseInt(params.page) : 1;
		this.catPage = this.props.match.params.category;

		if (this.catPage) {
			this.home = '/' + this.props.match.params.category;
			articleService
				.getCArticles(this.props.match.params.category, (this.curPage - 1) * this.prPage, this.prPage)
				.then((res) => {
					this.articles = res.articles;
					this.nTotal = res.total;
					this.loaded = true;
				});
		} else {
			this.home = '/';
			articleService.getFrontPageArticles((this.curPage - 1) * this.prPage, this.prPage).then((res) => {
				this.articles = res.articles;
				this.nTotal = res.total;
				this.loaded = true;
			});
		}
	}
}

class ArticleRoute extends Component<{ match: { params: { category: string, article_id: string } } }> {
	article: Object = {};
	comments = [];

	render() {
		if (this.article['error']) {
			return <Redirect to="/404" />;
		}
		return (
			<div className="contentWrapper">
				<ArticlePage
					article={this.article}
					article_id={parseInt(this.props.match.params.article_id)}
					category={this.props.match.params.category}
					preview={false}
					likeHandler={this.like}
					deleteHandler={this.delete}
				/>
				<br />
				<ArticleComments
					comments={this.comments}
					likeHandler={this.likeComment}
					postHandler={this.postComment}
				/>
			</div>
		);
	}

	delete() {
		articleService.deleteArticle(parseInt(this.props.match.params.article_id)).then((res) => {
			if (res['result'] == 'success') {
				(document.querySelector('#deleteDialog'): any).className = 'modal';
				setTimeout(() => ((document.querySelector('#deleteDialog'): any).className = 'modal fade'), 500);
				history.push('/');
			}
		});
	}

	like() {
		(document.querySelector('#likebtn'): any).setAttribute('disabled', 'true');
		this.article.likes++;
		articleService.like(this.article.article_id).then((res) => {
			if (!res['error'] && res['result'] == 'success') {
				this.article.likes = res['likes'];
			} else {
				(document.querySelector('#likebtn'): any).removeAttribute('disabled');
				this.article.likes--;
			}
		});
	}

	postComment(comment: Object, callback: boolean => mixed) {
		commentService.postComment(parseInt(this.props.match.params.article_id), comment).then((res) => {
			if (res['result'] == 'success') {
				this.comments = [
					{
						name: comment.name,
						body: comment.body,
						likes: 0,
						comment_id: res['comment_id'],
						time: new Date(Date.now()).toISOString()
					}
				].concat(this.comments);
				callback(true);
			} else {
				callback(false);
			}
		});
	}

	likeComment(comment_id: number) {
		(document.querySelector('#likebtn' + comment_id): any).setAttribute('disabled', 'true');
		(this.comments.find((e) => e.comment_id == comment_id): any).likes++;
		commentService.likeComment(comment_id).then((res) => {
			if (res['result'] == 'success') {
				(this.comments.find((e) => e.comment_id == comment_id): any).likes = res['likes'];
			} else {
				(document.querySelector('#likebtn' + comment_id): any).removeAttribute('disabled');
				(this.comments.find((e) => e.comment_id == comment_id): any).likes--;
			}
		});
	}

	mounted() {
		articleService
			.getArticle(this.props.match.params.category, parseInt(this.props.match.params.article_id))
			.then((article) => (this.article = article));
		commentService
			.getComments(parseInt(this.props.match.params.article_id))
			.then((comments) => (this.comments = comments));
	}
}

class EditorRoute extends Component<{ match: { params: { category: string, article_id: string } } }> {
	categories = [];
	importances = [];
	exists = false;
	p = null;
	editor;

	constructor(props) {
		super(props);
		this.editor = React.createRef();
	}

	render() {
		return (
			<div className="contentWrapper">
				<ArticleEditor
					categories={this.categories}
					ref={this.editor}
					importances={this.importances}
					back={
						this.props.match.params.category && this.props.match.params.article_id ? (
							'/' + this.props.match.params.category + '/' + this.props.match.params.article_id
						) : null
					}
					submit={this.submit}
				/>
			</div>
		);
	}

	mounted() {
		this.exists = this.props.match.params.category && this.props.match.params.article_id;
		if (this.exists) {
			articleService
				.getArticle(this.props.match.params.category, parseInt(this.props.match.params.article_id))
				.then((article) => ((this.editor.current: any).article = article));
		}
		categoryService.getCategories().then((categories) => (this.categories = categories));
		importanceService.getImportances().then((importances) => (this.importances = importances));
	}

	submit(article: Object, callback: () => mixed) {
		if (this.exists) {
			articleService.updateArticle(parseInt(this.props.match.params.article_id), article).then((res) => {
				let path: string = (this.categories.find((e) => e.category_id == article.category_id): any).path;
				if (res['result'] == 'success') {
					history.push('/' + path + '/' + this.props.match.params.article_id);
				} else {
					callback();
				}
			});
		} else {
			articleService.postArticle(article).then((res) => {
				let path: string = (this.categories.find((e) => e.category_id == article.category_id): any).path;
				if (res['result'] == 'success') {
					history.push('/' + path + '/' + res['article_id']);
				} else {
					callback();
				}
			});
		}
	}
}

class NoMatch extends Component {
	render() {
		return (
			<div align="center" className="contentWrapper">
				<br />
				<br />
				<h1>404 Page not found</h1>
			</div>
		);
	}
}

const root = document.getElementById('root');
if (root)
	ReactDOM.render(
		<Router history={history}>
			<div>
				<Menu />
				<Alert />
				<LiveFeed />
				<Switch>
					<Route exact path="/" component={ArticleListRoute} />
					<Route exact path="/register" component={EditorRoute} />
					<Route exact path="/404" component={NoMatch} />
					<Route exact path="/:category" component={ArticleListRoute} />
					<Route exact path="/:category/:article_id" component={ArticleRoute} />
					<Route exact path="/:category/:article_id/edit" component={EditorRoute} />
					<Redirect from="*" to="/404" />
				</Switch>
				<Footer />
			</div>
		</Router>,
		root
	);
