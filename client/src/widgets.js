// @flow
/* eslint eqeqeq: "off" */

import * as React from 'react';
import { Component } from 'react-simplified';
import { Link, NavLink } from 'react-router-dom';
import { type Article, type Category, type Importance, articleService, categoryService } from './services';

function formatDate(isoString: string): string {
	let d: Date = new Date(Date.parse(isoString));
	let s: string = d.toLocaleString();
	return s.substring(0, s.length - 3);
}

export class Menu extends Component {
	categories = [];

	render() {
		return (
			<div>
				<nav
					className={
						'navbar fixed-top navbar-expand-lg navbar-dark bg-dark shadow p-3 justify-content-between'
					}
				>
					<button
						className="navbar-toggler"
						type="button"
						data-toggle="collapse"
						data-target="#navToggler"
						aria-controls="navToggler"
						aria-expanded="false"
						aria-label="Toggle navigation"
					>
						<span className="navbar-toggler-icon" />
					</button>
					<div className="collapse navbar-collapse" id="navToggler">
						<ul className={'navbar-nav'}>
							<li className={'nav-item'}>
								<NavLink className="nav-link" activeStyle={{ color: 'white' }} exact to="/">
									Home
								</NavLink>
							</li>
							{this.categories.map((category) => (
								<li className={'nav-item'} key={category.path}>
									<NavLink
										className="nav-link"
										activeStyle={{ color: 'white' }}
										exact
										to={'/' + category.path}
									>
										{category.name}
									</NavLink>
								</li>
							))}
						</ul>
					</div>
					<span className="navbar-brand logo">
						<Link to="/">The Daily Node</Link>
					</span>
					<ul className="navbar-nav right">
						<li className={'nav-item'}>
							<NavLink className="nav-link" activeStyle={{ color: 'white' }} exact to="/register">
								Write a story
							</NavLink>
						</li>
					</ul>
				</nav>
				<div id="buffer" />
			</div>
		);
	}

	mounted() {
		categoryService.getCategories().then((c) => (this.categories = c));
	}
}

export class Footer extends Component {
	render() {
		return (
			<footer className="page-footer">
				<div align="center">	
					<span>Copyright &copy; 2018 Bjørnar Østtveit All Rights Reserved.</span>
				</div>
			</footer>
		);
	}
}

export class ArticleCard extends Component<{ article: Article, preview: boolean }> {
	render() {
		let card = (
			<div className="card articlecard">
				<img
					className="card-img-top"
					src={this.props.article.img}
					style={{ objectPosition: this.props.article.img_x + '% ' + this.props.article.img_y + '%' }}
				/>
				<div className="card-body">
					<h3 className="card-title">{this.props.article.title}</h3>
					<span className="card-text">{this.props.article.likes} likes</span>
					<br />
					<span className="card-text">{formatDate(this.props.article.created)}</span>
				</div>
			</div>
		);

		if (this.props.preview) {
			return card;
		}
		return <Link to={'/' + this.props.article.cName + '/' + this.props.article.article_id}>{card}</Link>;
	}
}

export class ArticleColumns extends Component<{ articles: Article[] }> {
	render() {
		return (
			<div>
				<div className="column">
					{this.props.articles
						.filter((e, i) => i % 3 == 0)
						.map((article) => <ArticleCard key={article.article_id} article={article} preview={false} />)}
				</div>
				<div className="column">
					{this.props.articles
						.filter((e, i) => i % 3 == 1)
						.map((article) => <ArticleCard key={article.article_id} article={article} preview={false} />)}
				</div>
				<div className="column">
					{this.props.articles
						.filter((e, i) => i % 3 == 2)
						.map((article) => <ArticleCard key={article.article_id} article={article} preview={false} />)}
				</div>
			</div>
		);
	}
}

export class PageButtons extends Component<{
	home: string,
	nElements: number,
	prPage: number,
	curPage: number,
	history: any
}> {
	b = 3;

	render() {
		let c = this.props.curPage;
		let n = this.props.nElements || null;
		let p = this.props.prPage;

		if (n) {
			let last = Math.ceil(n / p);
			let nLeft = c > this.b + 1 ? this.b : c - 1;
			let left = [ ...Array(nLeft).keys() ].map((i) => c - nLeft + i);
			let nRight = last - c > this.b + 1 ? this.b : last - c;
			let right = [ ...Array(nRight).keys() ].map((i) => c + i + 1);

			if (left.length > 0 || right.length > 0) {
				return (
					<div className="pageButtons">
						{c > 1 && (
							<Link className="pageLink" to={'?page=' + (c - 1)}>
								&#10094;
							</Link>
						)}
						{left[0] > 1 && (
							<div>
								<button
									className={'btn btn-dark mr-1'}
									onClick={() => this.props.history.push(this.props.home)}
								>
									1
								</button>
								<span className="buttonDots">...</span>
							</div>
						)}
						{left.map((i) => (
							<button
								key={i}
								className={'btn btn-dark mr-1'}
								onClick={() => this.props.history.push('?page=' + i)}
							>
								{i}
							</button>
						))}
						<button disabled={true} className={'btn btn-dark mr-1'}>
							{c}
						</button>
						{right.map((i) => (
							<button
								key={i}
								className={'btn btn-dark mr-1'}
								onClick={() => this.props.history.push('?page=' + i)}
							>
								{i}
							</button>
						))}
						{right[right.length - 1] < last && (
							<div>
								<span className="buttonDots">...</span>
								<button
									className={'btn btn-dark mr-1'}
									onClick={() => this.props.history.push('?page=' + last)}
								>
									{last}
								</button>
							</div>
						)}
						{c < last && (
							<Link className="pageLink" to={'?page=' + (c + 1)}>
								&#10095;
							</Link>
						)}
					</div>
				);
			} else {
				return <div />;
			}
		}
		return <div />;
	}
}

export class ArticlePage extends Component<{
	article: Article,
	category: ?string,
	article_id: ?number,
	preview: boolean,
	likeHandler: ?any,
	deleteHandler: ?any
}> {
	render() {
		return (
			<div>
				<div className="card articleContent">
					<img
						className="card-img-top"
						src={this.props.article ? this.props.article.img : ''}
						style={{
							objectPosition:
								(this.props.article ? this.props.article.img_x : 50) +
								'% ' +
								(this.props.article ? this.props.article.img_y : 50) +
								'%'
						}}
					/>
					<span>
						<i>&nbsp;Written: {this.props.article ? formatDate(this.props.article.created) : ''}</i>
						{this.props.article &&
						this.props.article.updated && (
							<i className="float-right">
								Updated: {this.props.article ? formatDate(this.props.article.updated) : ''}&nbsp;
							</i>
						)}
					</span>
					<div className="card-body">
						<h1 className="card-title">{this.props.article ? this.props.article.title : ''}</h1>
						{!this.props.preview && (
							<button id="likebtn" className="btn btn-secondary" onClick={this.props.likeHandler}>
								Like {this.props.article ? this.props.article.likes : ''}
							</button>
						)}
						{!this.props.preview && (
							<div className="dropdown float-right">
								<button
									className="btn btn-secondary dropdown-toggle"
									type="button"
									id="dropdownMenuButton"
									data-toggle="dropdown"
									aria-haspopup="true"
									aria-expanded="false"
								>
									Options
								</button>
								<div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<Link
										className="dropdown-item"
										to={
											'/' +
											String(this.props.category) +
											'/' +
											String(this.props.article_id) +
											'/edit'
										}
									>
										Edit article
									</Link>
									<a className="dropdown-item" data-toggle="modal" data-target="#deleteDialog">
										Delete article
									</a>
								</div>
							</div>
						)}
						<hr />
						{this.props.article &&
						!!this.props.article.html && (
							<div
								className="articleBody"
								dangerouslySetInnerHTML={{ __html: this.props.article.body }}
							/>
						)}
						{this.props.article &&
						!this.props.article.html && (
							<div className="articleBody">
								{this.props.article.body &&
									this.props.article.body
										.split('\n')
										.filter((e) => e != '')
										.map((paragraph, i) => <p key={i}>{paragraph}</p>)}
							</div>
						)}
					</div>
				</div>
				{!this.props.preview && (
					<div id="deleteDialog" className="modal fade">
						<div className="modal-dialog">
							<div className="modal-content">
								<div className="modal-header">
									<h5>Confirm</h5>
									<button type="button" className="close" data-dismiss="modal">
										&times;
									</button>
								</div>
								<div className="modal-body">
									<p>
										Are you sure you want to delete '{this.props.article ? this.props.article.title : ''}'?
									</p>
								</div>
								<div className="modal-footer">
									<button
										type="button"
										className="btn btn-danger"
										data-dismiss="modal"
										onClick={this.props.deleteHandler}
									>
										Yes
									</button>
									<button
										type="button"
										className="btn btn-secondary btn-default"
										data-dismiss="modal"
									>
										No
									</button>
								</div>
							</div>
						</div>
					</div>
				)}
			</div>
		);
	}
}

export class ArticleComments extends Component<{ comments: any, postHandler: any, likeHandler: any }> {
	disabled: boolean = false;
	comment: Object = { name: '', body: '' };
	commentInput: React.ElementRef<any>;

	constructor(props: any) {
		super(props);
		this.commentInput = React.createRef();
	}

	inputChange(e: Object) {
		const target = e.target;
		const value = target.value;
		const name = target.name;

		this.comment[name] = value;
	}

	render() {
		return (
			<div className="container border rounded p-3">
				<h3 className="mb-2">Write a comment</h3>
				<div>
					<div className="input-group mb-2">
						<div className="input-group-prepend">
							<span className="input-group-text" id="basic-addon1">
								@
							</span>
						</div>
						<input
							disabled={this.disabled}
							onChange={this.inputChange}
							value={this.comment.name}
							name="name"
							type="text"
							id="nameInput"
							className="form-control"
							placeholder="Nickname"
							aria-label="Nickname"
							aria-describedby="basic-addon1"
						/>
					</div>
					<textarea
						ref={this.commentInput}
						disabled={this.disabled}
						onChange={this.inputChange}
						name="body"
						value={this.comment.body}
						className="form-control"
						id="commentInput"
						placeholder="Comment"
					/>
					<button
						disabled={this.disabled}
						className="btn btn-primary mt-2"
						id="postComment"
						onClick={this.postComment}
					>
						Post
					</button>
				</div>
				{this.props.comments && this.props.comments.length > 0 && <hr />}
				<ul className="list-group">
					{this.props.comments && this.props.comments.length > 0 ? (
						this.props.comments.map((comment) => (
							<li className="list-group-item" key={comment.comment_id}>
								<b>{comment.name}</b>
								<i className="float-right">{formatDate(comment.time)}</i>
								{comment.body.split('\n').filter((e) => e != '').map((paragraph, i) => (
									<p className="mb-0" key={i}>
										{paragraph}
									</p>
								))}
								<button
									className="btn btn-sm btn-secondary mt-2"
									id={'likebtn' + comment.comment_id}
									onClick={() => this.props.likeHandler(comment.comment_id)}
								>
									Like {comment.likes}
								</button>
							</li>
						))
					) : (
						''
					)}
				</ul>
			</div>
		);
	}

	postComment() {
		Alert.clear();

		let valid = true;

		if (this.comment.name.length < 3) {
			Alert.danger('Please enter a nickname, at least 3 characters long.');
			valid = false;
		}

		if (this.comment.body.length < 1) {
			Alert.danger('Please write a comment to submit.');
			valid = false;
		}

		if (valid) {
			this.disabled = true;
			this.props.postHandler(this.comment, (success: boolean) => {
				this.disabled = false;
				if (success) this.comment = { name: '', body: '' };
			});
			return true;
		} else {
			return false;
		}
	}

	mounted() {
		if (this.commentInput.current) {
			this.commentInput.current.addEventListener('keydown', (event: Object) => {
				if (event.key === 'Enter' && !event.shiftKey) {
					this.postComment();
				}
			});
		}
	}
}

export class ArticleEditor extends Component<{
	back: ?string,
	categories: Category[],
	importances: Importance[],
	submit: any
}> {
	article: Object = {
		created: new Date(Date.now()).toISOString(),
		title: '',
		img: '',
		img_x: 50,
		img_y: 50,
		category_id: 1,
		importance_id: 1,
		body: '',
		html: 0
	};

	disabled: boolean = false;

	render() {
		return (
			<div>
				<input
					disabled={this.disabled}
					name="title"
					value={this.article.title}
					onChange={this.inputChange}
					id="titleInput"
					className="form-control form-control-lg"
					type="text"
					placeholder="Title"
				/>

				<div className="form-inline mt-3 justify-content-between">
					<div className="input-group mr-3">
						<div className="input-group-prepend">
							<span className="input-group-text">Image url</span>
						</div>
						<input
							disabled={this.disabled}
							name="img"
							value={this.article.img}
							onChange={this.inputChange}
							id="imgInput"
							className="form-control"
							type="text"
							placeholder="http://"
						/>
					</div>

					<div className="input-group mr-3">
						<div className="input-group-prepend">
							<label className="input-group-text" htmlFor="categoryInput">
								Category
							</label>
						</div>
						<select
							disabled={this.disabled}
							name="category_id"
							value={this.article.category_id}
							onChange={this.inputChange}
							id="categoryInput"
							className="custom-select"
						>
							{this.props.categories.map((c) => (
								<option key={c.category_id} value={c.category_id}>
									{c.name}
								</option>
							))}
						</select>
					</div>

					<div className="input-group">
						<div className="input-group-prepend">
							<label className="input-group-text" htmlFor="importanceInput">
								Importance
							</label>
						</div>
						<select
							disabled={this.disabled}
							name="importance_id"
							value={this.article.importance_id}
							onChange={this.inputChange}
							id="importanceInput"
							className="custom-select"
						>
							{this.props.importances.map((i) => (
								<option key={i.importance_id} value={i.importance_id}>
									{i.name}
								</option>
							))}
						</select>
					</div>
				</div>

				<div className="input-group mt-3">
					Body:&nbsp;&nbsp;
					<div className="form-check">
						<input
							disabled={this.disabled}
							name="html"
							checked={!!this.article.html}
							onChange={this.inputChange}
							type="checkbox"
							className="form-check-input"
							id="htmlInput"
						/>
						<label className="form-check-label" htmlFor="htmlInput">
							HTML
						</label>
					</div>
					<textarea
						disabled={this.disabled}
						name="body"
						onChange={this.inputChange}
						value={this.article.body}
						className="form-control"
						placeholder="Body"
						id="bodyInput"
					/>
				</div>

				<div className="form-inline mt-3">
					<div className="form-group XYInput">
						<label htmlFor="imgXInput">Image x position:</label>
						<input
							disabled={this.disabled}
							name="img_x"
							onChange={this.inputChange}
							value={this.article.img_x}
							type="range"
							min="0"
							max="100"
							id="imgXInput"
							className="form-control-range"
						/>
					</div>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<div className="form-group XYInput">
						<label htmlFor="imgYInput">Image y position:</label>
						<input
							disabled={this.disabled}
							name="img_y"
							onChange={this.inputChange}
							value={this.article.img_y}
							type="range"
							min="0"
							max="100"
							id="imgYInput"
							className="form-control-range"
						/>
					</div>
				</div>
				<br />
				<h4>
					<b>Preview:</b>
				</h4>
				<hr />

				<ArticlePage
					preview={true}
					article_id={null}
					category={null}
					likeHandler={null}
					deleteHandler={null}
					article={this.article}
				/>
				<div style={{ position: 'fixed', left: 'calc(50% + 510px)', top: '150px' }}>
					<h5 className="ml-3 mb-0">
						<b>Preview:</b>
					</h5>
					<ArticleCard article={this.article} preview={true} />
				</div>

				<hr />

				{this.props.back && (
					<span>
						<Link disabled={this.disabled} to={String(this.props.back)} className="btn btn-secondary">
							Back
						</Link>&nbsp;&nbsp;
					</span>
				)}
				<button disabled={this.disabled} id="articleSubmit" onClick={this.submit} className="btn btn-primary">
					Submit
				</button>
			</div>
		);
	}

	submit() {
		Alert.clear();

		let valid = true;

		if (this.article.title.length < 5) {
			Alert.danger('Please enter a valid title, at least 5 characters long.');
			valid = false;
		}

		let urlPattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
		if (!urlPattern.test(this.article.img)) {
			Alert.danger('Please enter a valid image url.');
			valid = false;
		}

		if (this.article.body.length < 100) {
			Alert.danger('Please write an article body of at least 100 characters.');
			valid = false;
		}

		if (valid) {
			this.disabled = true;
			this.props.submit(this.article, () => {
				this.disabled = false;
			});
			return true;
		} else {
			return false;
		}
	}

	inputChange(e: Object) {
		const target = e.target;
		const value = target.type === 'checkbox' ? (target.checked ? 1 : 0) : target.value;
		const name = target.name;

		this.article[name] = value;
	}
}

export class LiveFeed extends Component {
	articles: Article[] = [];
	visible: boolean = true;
	n: number = 10;
	e: any = null;
	es: Array<Object> = [];
	c: number = 0;
	t: any = null;

	render() {
		return (
			<div id="liveView">
				<div id="liveContainer">
					{this.articles.map((article, i) => (
						<div key={article.article_id} id={'lb' + i} className="liveBox">
							<Link to={'/' + article.cName + '/' + article.article_id}>
								<div className="liveInnerBox">
									<p>{article.title}</p>
									<i>{formatDate(article.created)}</i>
								</div>
							</Link>
						</div>
					))}
				</div>
				<div id="liveButton" onClick={this.hide} />
			</div>
		);
	}

	hide() {
		if (this.visible) {
			this.visible = false;
			(document.querySelector('#liveContainer'): any).className = 'hideLive';
			(document.querySelector('#liveContainer'): any).childNodes.forEach(
				(e) => (e.className = 'liveBox hideLive')
			);
			setTimeout(() => ((document.querySelector('#liveContainer'): any).style.opacity = '0'), 500);
		} else {
			this.visible = true;
			(document.querySelector('#liveContainer'): any).style.opacity = '1';
			(document.querySelector('#liveContainer'): any).className = '';
			(document.querySelector('#liveContainer'): any).childNodes.forEach((e) => (e.className = 'liveBox'));
		}
	}

	setPos() {
		if (this.es[0].offsetLeft >= -this.es[0].clientWidth) {
			this.e.style.marginLeft = this.es[0].offsetLeft - 1 + 'px';
		} else {
			this.e.style.marginLeft = '0px';
			let l = this.es[0];
			this.e.removeChild(l);
			this.e.appendChild(l);
			if (this.c < this.n - 1) {
				this.c++;
			} else {
				this.updateItems();
				this.c = 0;
			}
		}
	}

	updateItems() {
		clearInterval(this.t);
		articleService.getLiveArticles(this.n).then((res) => {
			this.articles = res.articles;
			this.e = (document.querySelector('#liveContainer'): any);
			this.es = this.e.childNodes;
			this.t = setInterval(this.setPos, 20);
		});
	}

	mounted() {
		this.updateItems();
	}
}

export class Alert extends Component {
	alerts: { text: React.Node, type: string }[] = [];

	ch = 78;

	render() {
		return (
			<div>
				{this.alerts.map((alert, i) => (
					<div
						key={i}
						style={{ top: this.ch + 50 * i + 'px' }}
						className={'alert alert-' + alert.type}
						role="alert"
					>
						{alert.text}
						<button
							className="close"
							onClick={() => {
								this.alerts.splice(i, 1);
							}}
						>
							&times;
						</button>
					</div>
				))}
			</div>
		);
	}

	static clear() {
		for (let instance of Alert.instances()) instance.alerts = [];
	}

	static success(text: React.Node) {
		// To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
		setTimeout(() => {
			for (let instance of Alert.instances()) instance.alerts.push({ text: text, type: 'success' });
		});
	}

	static info(text: React.Node) {
		// To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
		setTimeout(() => {
			for (let instance of Alert.instances()) instance.alerts.push({ text: text, type: 'info' });
		});
	}

	static warning(text: React.Node) {
		// To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
		setTimeout(() => {
			for (let instance of Alert.instances()) instance.alerts.push({ text: text, type: 'warning' });
		});
	}

	static danger(text: React.Node) {
		// To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
		setTimeout(() => {
			for (let instance of Alert.instances()) instance.alerts.push({ text: text, type: 'danger' });
		});
	}
}
