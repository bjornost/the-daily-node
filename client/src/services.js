// @flow
import axios from 'axios';
axios.interceptors.response.use((response) => response.data);

export type ArticleList = {
	articles: Article[];
	total: number;
}

export type Article = {
	article_id: number;
	title: string;
	body: string;
	html: number;
	created: string;
	updated: string;
	img: string;
	img_x: number;
	img_y: number;
	likes: number;
	category_id: number;
	importance_id: number;
	cName: string;
}

class ArticleService {
	getFrontPageArticles(skip: number, limit: number): Promise<ArticleList> {
		return axios.get('/api/articles?importance=2&skip=' + skip + '&limit=' + limit);
	}

	getLiveArticles(n: number): Promise<ArticleList> {
		return axios.get('/api/articles?limit=' + n);
	}

	getCArticles(c: string, skip: number, limit: number): Promise<ArticleList> {
		return axios.get('/api/articles?category=' + c + '&skip=' + skip + '&limit=' + limit);
	}

	getArticle(c: string, article_id: number): Promise<Article> {
		return axios.get('/api/categories/' + c + '/' + article_id);
	}

	like(article_id: number) {
		return axios.get('/api/articles/' + article_id + '/like');
	}

	postArticle(data: Object) {
		return axios.post('/api/articles', data);
	}

	updateArticle(article_id: number, data: Object) {
		return axios.put('/api/articles/' + article_id, data);
	}

	deleteArticle(article_id: number) {
		return axios.delete('/api/articles/' + article_id);
	}
}
export let articleService = new ArticleService();

class CommentService {
	getComments(article_id: number) {
		return axios.get('/api/articles/' + article_id + '/comments');
	}

	likeComment(comment_id: number) {
		return axios.get('/api/comments/' + comment_id + '/like');
	}

	postComment(article_id: number, data: any) {
		return axios.post('/api/articles/' + article_id + '/comments', data);
	}
}
export let commentService = new CommentService();

export type Category = {
	category_id: number;
	name: string;
	path: string;
};

class CategoryService {
	getCategories(): Promise<Category[]> {
		return axios.get('/api/categories');
	}
}
export let categoryService = new CategoryService();

export type Importance = {
	importance_id: number;
	name: string;
};

class ImportanceService {
	getImportances(): Promise<Importance[]> {
		return axios.get('/api/importances');
	}
}
export let importanceService = new ImportanceService();
