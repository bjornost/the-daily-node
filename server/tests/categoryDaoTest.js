// @flow

import CategoryDao from '../src/dao/categoryDao';
import mysql2 from 'mysql2'
import fs from 'fs';

const pool: mysql2.Pool = mysql2.createPool({
	host: 'mysql',
	user: 'root',
	password: '',
	database: 'bjornost',
	waitForConnections: true,
	connectionLimit: 10,
	queueLimit: 0,
	multipleStatements: true
});

let categoryDao = new CategoryDao(pool);

beforeAll(done => {
    let structure = fs.readFileSync("./tests/db_structure.sql", "utf8");
    pool.query(structure, (err, res) => {
        let data = fs.readFileSync("./tests/category_data.sql", "utf8");
        pool.query(data, done);
    });
});

test("get categories from db", done => {
    categoryDao.getCategories((status: number, rows: Object) => {
        expect(status).toBe(200);
        expect(rows.length).toBe(4);
        expect(rows[0].name).toBe("News");
        done();
    })
});