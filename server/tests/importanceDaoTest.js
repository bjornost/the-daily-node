// @flow

import ImportanceDao from '../src/dao/importanceDao';
import mysql2 from 'mysql2'
import fs from 'fs';

const pool: mysql2.Pool = mysql2.createPool({
	host: 'mysql',
	user: 'root',
	password: '',
	database: 'bjornost',
	waitForConnections: true,
	connectionLimit: 10,
	queueLimit: 0,
	multipleStatements: true
});

let importanceDao = new ImportanceDao(pool);

beforeAll(done => {
    let structure = fs.readFileSync("./tests/db_structure.sql", "utf8");
    pool.query(structure, (err, res) => {
        let data = fs.readFileSync("./tests/importance_data.sql", "utf8");
        pool.query(data, done);
    });
});

test("get importances from db", done => {
    importanceDao.getImportances((status: number, rows: Object) => {
        expect(status).toBe(200);
        expect(rows.length).toBe(2);
        expect(rows[1].name).toBe("Important");
        done();
    })
});