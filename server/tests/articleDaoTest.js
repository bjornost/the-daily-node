// @flow

import ArticleDao from '../src/dao/articleDao';
import mysql2 from 'mysql2';
import fs from 'fs';

const pool: mysql2.Pool = mysql2.createPool({
	host: 'mysql',
	user: 'root',
	password: '',
	database: 'bjornost',
	waitForConnections: true,
	connectionLimit: 10,
	queueLimit: 0,
	multipleStatements: true
});

let articleDao = new ArticleDao(pool);

beforeAll((done) => {
	let sql = fs.readFileSync('./tests/db_structure.sql', 'utf8');
	pool.query(sql, (err, res) => {
		let c = fs.readFileSync('./tests/category_data.sql', 'utf8');
		pool.query(c, (err2, res2) => {
			let i = fs.readFileSync('./tests/importance_data.sql', 'utf8');
			pool.query(i, (err3, res3) => {
				let a = fs.readFileSync('./tests/article_data.sql', 'utf8');
				pool.query(a, done);
			});
		});
	});
});

test('get all articles', (done) => {
	articleDao.getArticles('created', 'DESC', 5, 0, 'news', 2, (status: number, rows: Object) => {
		expect(status).toBe(200);
		expect(rows[0].length).toBe(5);
		expect(rows[1][0].a).toBeGreaterThan(5);
		expect(rows[0][0].title).toBe('wat ze fak');
		expect(rows[0][0].cName).toBe('news');
		expect(rows[0][0].importance_id).toBe(2);
		done();
	});
});

test('get one article', (done) => {
	articleDao.getArticle(2, (status: number, rows: Object) => {
		expect(status).toBe(200);
		expect(rows.length).toBe(1);
		expect(rows[0].title).toBe('William skyter Øivind');
		done();
	});
});

test('get one article by category', (done) => {
	articleDao.getArticlebyCategory('news', 1, (status: number, rows: Object) => {
		expect(status).toBe(200);
		expect(rows.length).toBe(1);
		expect(rows[0].title).toBe('Tihlde beste linjeforening i 2018');
		expect(rows[0].category_id).toBe(1);
		done();
	});
});

test('insert one article', (done) => {
	articleDao.insertArticle(
		'test article 1',
		'http://www...png',
		50,
		50,
		'this is a test article',
		0,
		1,
		2,
		(status: number, rows: Object) => {
			expect(status).toBe(200);
			expect(rows.insertId).toBe(16);
			expect(rows.affectedRows).toBe(1);
			done();
		}
	);
});

test('delete one article', (done) => {
	articleDao.deleteArticle(8, (status: number, rows: Object) => {
		expect(status).toBe(200);
		expect(rows[1].affectedRows).toBe(1);
		done();
	});
});

test('update one article', (done) => {
	articleDao.updateArticle(
		6,
		'test oppdatering',
		'http://www...png',
		50,
		50,
		'test innhold',
		0,
		1,
		2,
		(status: number, rows: Object) => {
			expect(status).toBe(200);
			expect(rows.affectedRows).toBe(1);
			done();
		}
	);
});

test('like one article', (done) => {
	articleDao.likeArticle(7, (status: number, rows: Object) => {
		expect(status).toBe(200);
		expect(rows[0].affectedRows).toBe(1);
		expect(rows[1][0].likes).toBe(6);
		done();
	});
});
