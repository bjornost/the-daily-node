// @flow

import CommentDao from '../src/dao/commentDao';
import mysql2 from 'mysql2';
import fs from 'fs';

const pool: mysql2.Pool = mysql2.createPool({
	host: 'mysql',
	user: 'root',
	password: '',
	database: 'bjornost',
	waitForConnections: true,
	connectionLimit: 10,
	queueLimit: 0,
	multipleStatements: true
});

let commentDao = new CommentDao(pool);

beforeAll((done) => {
	let sql = fs.readFileSync('./tests/db_structure.sql', 'utf8');
	pool.query(sql, (err, res) => {
		let c = fs.readFileSync('./tests/category_data.sql', 'utf8');
		pool.query(c, (err2, res2) => {
			let i = fs.readFileSync('./tests/importance_data.sql', 'utf8');
			pool.query(i, (err3, res3) => {
				let a = fs.readFileSync('./tests/article_data.sql', 'utf8');
				pool.query(a, (err4, res4) => {
                    let co = fs.readFileSync('./tests/comment_data.sql', 'utf8');
                    pool.query(co, done);
                });
			});
		});
	});
});

test('get comments on an article', (done) => {
	commentDao.getComments(1, (status: number, rows: Object) => {
		expect(status).toBe(200);
		expect(rows.length).toBe(3);
		expect(rows[0].name).toBe("bjørnar");
        expect(rows[0].likes).toBeGreaterThan(rows[1].likes);
		done();
	});
});

test('insert one comment', (done) => {
	commentDao.insertComment(2, "bjørnar", "kul artikkel", (status: number, rows: Object) => {
		expect(status).toBe(200);
        expect(rows.affectedRows).toBe(1);
        expect(rows.insertId).toBe(43);
		done();
	});
});

test('like a comment', (done) => {
	commentDao.likeComment(12, (status: number, rows: Object) => {
		expect(status).toBe(200);
		expect(rows[0].affectedRows).toBe(1);
        expect(rows[1][0].likes).toBe(2);
		done();
	});
});