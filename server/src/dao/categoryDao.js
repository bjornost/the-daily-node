// @flow

const Dao = require('./dao');

module.exports = class CategoryDao extends Dao {
	getCategories(callback: (number, Object) => mixed) {
		let query: string = 'SELECT * FROM Category';
		super.query(query, [], callback);
	}
};
