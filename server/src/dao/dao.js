// @flow

import mysql2 from 'mysql2';

module.exports = class Dao {
    pool: mysql2.Pool;

    constructor(pool: mysql2.Pool) {
        this.pool = pool;
    }

    query(sql: string, params: Array<mixed>, callback: (number, Object) => mixed) {
        this.pool.getConnection((err,connection) => {
            if (err) {
                console.log(err);
                callback(500, {error: "could not get connection"});
            } else {
                connection.query(sql, params, (err,rows) => {
                    if (err) {
                        console.log(err);
                        callback(400, {error: "invalid query"});
                    } else {
                        callback(200, rows);
                    }
                });
                this.pool.releaseConnection(connection);
            }
        });
    }

}