// @flow

const Dao = require('./dao');

module.exports = class ImportanceDao extends Dao {
	getImportances(callback: (number, Object) => mixed) {
		let query: string = 'SELECT * FROM Importance';
		super.query(query, [], callback);
	}
};
