// @flow

const Dao = require('./dao');

module.exports = class CommentDao extends Dao {
	getComments(article_id: number, callback: (number, Object) => mixed) {
		let query: string = 'SELECT * FROM Comment WHERE article_id = ? ORDER BY likes DESC, time DESC';
		let params: Array<mixed> = [ article_id ];
		super.query(query, params, callback);
	}

	insertComment(article_id: number, name: string, body: string, callback: (number, Object) => mixed) {
		let query: string =
			'INSERT INTO Comment (comment_id,article_id,name,body,likes,time) VALUES(DEFAULT,?,?,?,DEFAULT,DEFAULT)';
		let params: Array<mixed> = [ article_id, name, body ];
		super.query(query, params, callback);
	}

	likeComment(comment_id: number, callback: (number, Object) => mixed) {
		let query: string =
			'UPDATE Comment SET likes=likes+1 WHERE comment_id=?; SELECT likes FROM Comment WHERE comment_id=?';
		let params: Array<mixed> = [ comment_id, comment_id ];
		super.query(query, params, callback);
	}
};
