// @flow

const Dao = require('./dao');

module.exports = class ArticleDao extends Dao {
	getArticles(
		sort: string,
		order: string,
		limit: number,
		skip: number,
		category: string|null,
		important: number|null,
		callback: (number, Object) => mixed
	) {
		let query1: string = 'SELECT Article.*, Category.path as cName FROM Article NATURAL JOIN Category';
		let query2: string = 'SELECT COUNT(*) as a FROM Article NATURAL JOIN Category';
		let params: Array<mixed> = [];
		let options: string = '';

		if (category || important) {
			options += ' WHERE';
		}

		if (category) {
			options += ' Category.path = ?';
			params.push(category);
		}

		if (category && important) {
			options += ' AND';
		}

		if (important) {
			options += ' importance_id = ?';
			params.push(important);
		}

		let options2: string = options;
		let params2: Array<mixed> = params.slice();

		options += ' ORDER BY ' + sort + ' ' + order + ' LIMIT ?, ?';
		params.push(skip);
		params.push(limit);

		params = params.concat(params2);

		let query: string = query1 + options + '; ' + query2 + options2;

		super.query(query, params, callback);
	}

	getArticle(article_id: number, callback: (number, Object) => mixed) {
		let query: string = 'SELECT * FROM Article WHERE article_id = ?';
		let params: Array<mixed> = [ article_id ];

		super.query(query, params, callback);
	}

	getArticlebyCategory(path: string, article_id: number, callback: (number, Object) => mixed) {
		let query: string = 'SELECT Article.* FROM Article NATURAL JOIN Category WHERE article_id = ? AND path = ?';
		let params: Array<mixed> = [ article_id, path ];

		super.query(query, params, callback);
	}

	insertArticle(
		title: string,
		img: string,
		img_x: number,
		img_y: number,
		body: string,
		html: number,
		category_id: number,
		importance_id: number,
		callback: (number, Object) => mixed
	) {
		let query: string =
			'INSERT INTO Article (article_id, title, img, img_x, img_y, body, html, created, updated, likes, category_id, importance_id) VALUES(DEFAULT,?,?,?,?,?,?,DEFAULT,DEFAULT,DEFAULT,?,?)';
		let params: Array<mixed> = [ title, img, img_x, img_y, body, html, category_id, importance_id ];

		super.query(query, params, callback);
	}

	deleteArticle(article_id: number, callback: (number, Object) => mixed) {
		let query: string = 'DELETE FROM Comment WHERE article_id = ?; DELETE FROM Article WHERE article_id = ?;';
		let params: Array<mixed> = [ article_id, article_id ];

		super.query(query, params, callback);
	}

	updateArticle(
		article_id: number,
		title: string,
		img: string,
		img_x: number,
		img_y: number,
		body: string,
		html: number,
		category_id: number,
		importance_id: number,
		callback: (number, Object) => mixed
	) {
		let query: string =
			'UPDATE Article SET title=?, body=?, html=?, img=?, img_x=?, img_y=?, category_id=?, importance_id=?, updated=NOW() WHERE article_id=?';
		let params: Array<mixed> = [ title, body, html, img, img_x, img_y, category_id, importance_id, article_id ];

		super.query(query, params, callback);
	}

	likeArticle(article_id: number, callback: (number, Object) => mixed) {
		let query: string =
			'UPDATE Article SET likes=likes+1 WHERE article_id=?; SELECT likes FROM Article WHERE article_id=?';
		let params: Array<mixed> = [ article_id, article_id ];

		super.query(query, params, callback);
	}
};
