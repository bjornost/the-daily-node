// @flow

import express from 'express';
import path from 'path';
import reload from 'reload';
import fs from 'fs';
import mysql2 from 'mysql2';
import ArticleDao from './dao/articleDao';
import CategoryDao from './dao/categoryDao';
import ImportanceDao from './dao/importanceDao';
import CommentDao from './dao/commentDao';

type Request = express$Request;
type Response = express$Response;

const public_path = path.join(__dirname, '/../../client/public');

const pool: mysql2.Pool = mysql2.createPool({
  host: 'localhost',
	user: 'bjornost',
	password: '-.,jegheter12345',
	database: 'bjornost',
	waitForConnections: true,
	connectionLimit: 10,
	queueLimit: 0,
	multipleStatements: true
});

let articleDao = new ArticleDao(pool);
let categoryDao = new CategoryDao(pool);
let importanceDao = new ImportanceDao(pool);
let commentDao = new CommentDao(pool);


let app = express();

app.use(express.static(public_path));
app.use(express.json());


app.get('/api/articles', (req: Request, res: Response) => {
  let sortIndex: number = parseInt(req.query.sort) || 0;
  let orderIndex: number = isNaN(parseInt(req.query.order)) ? 1 : parseInt(req.query.order);

  let sort: string = ["created", "updated", "likes"][sortIndex] || "created";
  let order: string = ["ASC", "DESC"][orderIndex] || "DESC";

  let limit: number = parseInt(req.query.limit) || 20;
  let skip: number = parseInt(req.query.skip) || 0;
  let category: any = req.query.category;
  let importance_id: any = parseInt(req.query.importance) || null;

  articleDao.getArticles(sort, order, limit, skip, category, importance_id, (status: number, rows: Object) => {
    let articles = rows[0];

    res.status(status);
    if (status == 200) {
      res.json({"articles":rows[0],"total":rows[1][0]["a"]});
    } else {
      res.json(rows);
    }
  });
});

app.post('/api/articles', (req: Request, res: Response) => {
  articleDao.insertArticle(req.body.title, req.body.img, req.body.img_x, req.body.img_y, req.body.body, req.body.html, req.body.category_id, req.body.importance_id, (status: number, rows: Object) => {
    res.status(status);
    if (status == 200) {
      res.json({ result: 'success', article_id: rows['insertId'] });
    } else {
      res.json(rows);
    }
  });
});

app.get('/api/articles/:article_id', (req: Request, res: Response) => {
  let sId: string = req.params.article_id;
  let article_id: number = parseInt(sId) || -1;
  if (article_id < 1) {
    res.json({error: "invalid query"});
    return;
  }

  articleDao.getArticle(article_id, (status: number, rows: Object) => {
    res.status(status);
    if (status == 200) {
      res.json(rows[0]);
    } else {
      res.json(rows);
    }
  });
});

app.delete('/api/articles/:article_id', (req: Request, res: Response) => {
  let sId: string = req.params.article_id;
  let article_id: number = parseInt(sId) || -1;
  if (article_id < 1) {
    res.json({error: "invalid query"});
    return;
  }

  articleDao.deleteArticle(article_id, (status: number, rows: Object) => {
      res.status(status);
      if (status == 200) {
        if (rows[1]['affectedRows'] > 0) {
          res.json({result:"success"})
        } else {
          res.json({result:"failed"})
        }
      } else {
        res.json(rows);
      }
  });
});

app.put('/api/articles/:article_id', (req: Request, res: Response) => {
  let sId: string = req.params.article_id;
  let article_id: number = parseInt(sId) || -1;
  if (article_id < 1) {
    res.json({error: "invalid query"});
    return;
  }

  articleDao.updateArticle(article_id, req.body.title, req.body.img, req.body.img_x, req.body.img_y, req.body.body, req.body.html, req.body.category_id, req.body.importance_id, (status: number, rows: Object) => {
      res.status(status);
      if (status == 200) {
        if (rows['affectedRows'] > 0) {
          res.json({result:"success"});
        } else {
          res.json({result:"failed"});
        }
      } else {
        res.json(rows);
      }
  });

});

app.get('/api/articles/:article_id/like', (req: Request, res: Response) => {
  let sId: string = req.params.article_id;
  let article_id: number = parseInt(sId) || -1;
  if (article_id < 1) {
    res.json({error: "invalid query"});
    return;
  }

  articleDao.likeArticle(article_id, (status: number, rows: Object) => {
      res.status(status);
      if (status == 200) {
        if (rows[0].affectedRows > 0) {
          res.json({result:"success", likes: rows[1][0]['likes']});
        } else {
          res.json({result:"failed"});
        }
      } else {
        res.json(rows);
      }
  });
});

app.get('/api/articles/:article_id/comments', (req: Request, res: Response) => {
  let sId: string = req.params.article_id;
  let article_id: number = parseInt(sId) || -1;
  if (article_id < 1) {
    res.json({error: "invalid query"});
    return;
  }

  commentDao.getComments(article_id, (status: number, rows: Object) => {
    res.status(status);
    res.json(rows);
  });
});

app.post('/api/articles/:article_id/comments', (req: Request, res: Response) => {
  let sId: string = req.params.article_id;
  let article_id: number = parseInt(sId) || -1;
  if (article_id < 1) {
    res.json({error: "invalid query"});
    return;
  }

  commentDao.insertComment(article_id, req.body.name, req.body.body, (status: number, rows: Object) => {
    res.status(status);
    if (status == 200) {
      if (rows['affectedRows'] > 0) {
				res.json({ result: 'success', comment_id: rows['insertId'] });
			} else {
				res.json({ result: 'failed' });
			}
    } else {
      res.json(rows);
    }
  });

});

app.get('/api/comments/:comment_id/like', (req: Request, res: Response) => {
  let sId: string = req.params.comment_id;
  let comment_id: number = parseInt(sId) || -1;
  if (comment_id < 1) {
    res.json({error: "invalid query"});
    return;
  }

  commentDao.likeComment(comment_id, (status: number, rows: Object) => {
    res.status(status);
    if (status == 200) {
      if (rows[0].affectedRows > 0) {
				res.json({ result: 'success', likes: rows[1][0]['likes'] });
			} else {
				res.json({ result: 'failed' });
			}
    } else {
      res.json(rows);
    }
  });

});

app.get('/api/categories', (req: Request, res: Response) => {
  categoryDao.getCategories((status: number, rows: Object) => {
    res.status(status);
    res.json(rows);
  });
});

app.get('/api/categories/:path/:article_id', (req: Request, res: Response) => {
  let sId: string = req.params.article_id;
  let article_id: number = parseInt(sId) || -1;
  if (article_id < 1 || !(article_id.toString() == sId)) {
    res.json({error: "invalid query"});
    return;
  }

  let path: string = req.params.path;

  articleDao.getArticlebyCategory(path, article_id, (status: number, rows: Object) => {
    res.status(status);
    if (status == 200) {
      if (rows.length > 0) {
        res.json(rows[0]);
      } else {
        res.json({"error": "article not found"})
      }
    } else {
      res.json(rows);
    }
  });
});

app.get('/api/importances', (req: Request, res: Response) => {
  importanceDao.getImportances((status: number, rows: Object) => {
    res.status(status);
    res.json(rows);
  });
})

app.get('*', (req: Request, res: Response) => {
  res.sendFile(path.join(__dirname+"../../../client/public/index.html"));
});

// Hot reload application when not in production environment
if (process.env.NODE_ENV !== 'production') {
  let reloadServer = reload(app);
  fs.watch(public_path, () => reloadServer.reload());
}

// The listen promise can be used to wait for the web server to start (for instance in your tests)
export let listen = new Promise<void>((resolve, reject) => {
  app.listen(3000, error => {
    if (error) reject(error.message);
    console.log('Server started');
    resolve();
  });
});
